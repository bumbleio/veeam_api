﻿#Function to get veeam api token

$apiserverame = #api set server name
$encodedcredentials = # base64 encoded credentials
$password = # password to set
$backupserverID = # serevr to are updating
$credentialsID = #


function GetVeeamToken()
{
Write-host "Creating VEEAM Rest API Powershell Session token" -ForegroundColor Yellow
$headers = @{ Authorization = "Basic $encodedcredentials" } # This is the base64 encoded password used for 
try
{
    $Veeamtoken = Invoke-WebRequest  -method Post -Uri "http://$apiservername:9399/api/sessionMngr/?v=latest" -Headers $headers 
}
catch
{
    Write-Host "failed to get veeam api token"
    write-host "Caught an exception:" -ForegroundColor Red
    write-host "Exception Type: $($_.Exception.GetType().FullName)" -ForegroundColor Red
    write-host "Exception Message: $($_.Exception.Message)" -ForegroundColor Red
    write-host "exiting script"
    exit
} 
$sessionId = $Veeamtoken.Headers.'X-RestSvcSessionId'
return $sessionId
}

#Function update veeamsql account password via api
function UpdatePassword($P1)
{
$date = get-date
$body = @"
<?xml version="1.0" encoding="utf-8"?>
<CredentialsInfo Type="Credentials" Href="http://$apiservername:9399/api/backupServers/$backupserverID/credentials/$credentialsID" xmlns="http://www.veeam.com/ent/v1.0" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
 <Id>$credentailsID</Id>
 <Description>updated on $date</Description>
 <Password>$password</Password>
</CredentialsInfo>
"@

Write-host "Updateing account details" -ForegroundColor Yellow
$headers1 = @{ 'X-RestSvcSessionId' = $P1 ; 'content-type' = 'application/xml' }
try
{
    Invoke-RestMethod  -method Put -Uri "http://$apiservername:9399/api/backupServers/$backupserverID/credentials/$credentialsID" -Headers $headers1 -Body $body
 
}
catch
{
    Write-Host "failed to update password"
    write-host "Caught an exception:" -ForegroundColor Red
    write-host "Exception Type: $($_.Exception.GetType().FullName)" -ForegroundColor Red
    write-host "Exception Message: $($_.Exception.Message)" -ForegroundColor Red
    write-host "exiting script"
    exit
}

Write-host "description updated and password" -ForegroundColor Yellow

}


#Main body
$veeamtoken = GetVeeamToken
UpdatePassword -p1 $veeamtoken




